package br.com.ejb.teste;

import javax.ejb.Remote;
import javax.ejb.Stateless;

@Stateless(name = "HelloStatelessWorldBean")
public class HelloStatelessWorldBean implements HelloStatelessWorld {

	@Override
	public String getHelloWorld() {
		return "Hello Stateless World!";
	}

}
