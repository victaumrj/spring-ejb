package br.com.ejb.teste;

public interface HelloStatefulWorld {
	
	int howManyTimes();
	
	String getHelloWorld();

}
