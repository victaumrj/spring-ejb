package br.com.ejb.teste;

import javax.ejb.Remote;
import javax.ejb.Stateful;

@Stateful(name = "HelloStatefulWorldBean")
@Remote(HelloStatefulWorld.class)
public class HelloStatefulWorldBean implements HelloStatefulWorld {

	private int homManyTimes = 0;
	
	@Override
	public int howManyTimes() {
		return homManyTimes;
	}

	@Override
	public String getHelloWorld() {
		homManyTimes++;
		return "Hello Stateful World";
	}

}
