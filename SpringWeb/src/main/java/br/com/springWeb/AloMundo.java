package br.com.springWeb;

import javax.interceptor.Interceptors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ejb.interceptor.SpringBeanAutowiringInterceptor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.servlet.ModelAndView;

import br.com.ejb.teste.HelloStatefulWorld;
import br.com.ejb.teste.HelloStatelessWorld;

@Controller("/AloMundo")
public class AloMundo {

	
	
	private HelloStatefulWorld helloStatefulWorld;
	
	@Autowired
	private HelloStatelessWorld helloStatelessWorld;

	@RequestMapping(value = "/index", method = RequestMethod.GET)	
	public ModelAndView index() {
		System.out.println("FOI");
		
		System.out.println("" + helloStatefulWorld.getHelloWorld());
		
		ModelAndView mv = new ModelAndView("index");	
		return mv;
	}
	
}
