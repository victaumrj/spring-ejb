package br.com.springWeb;

import java.util.Properties;

import javax.interceptor.Interceptors;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.ejb.interceptor.SpringBeanAutowiringInterceptor;
import org.springframework.web.client.RestTemplate;

import br.com.ejb.teste.HelloStatefulWorld;
import br.com.ejb.teste.HelloStatelessWorld;

@SpringBootApplication
@Interceptors(SpringBeanAutowiringInterceptor.class)
public class SpringWebApplication {

	
	public static void main(String[] args) {
		SpringApplication.run(SpringWebApplication.class, args);
		
//		Properties properties = new Properties();
//		properties.put("java.naming.factory.initial", "org.wildfly.naming.client.WildFlyInitialContextFactory");
//		properties.put("org.jboss.ejb.client.scoped.context", true);
//		
//		try {
//			InitialContext ctx = new InitialContext(properties);			
//			HelloStatefulWorld bean = (HelloStatefulWorld) ctx.lookup("ejb:SpringWeb/EJB//HelloStatefulWorldBean!br.com.ejb.teste.HelloStatelessWorld");			
//			System.out.println("^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#");
//			System.out.println(bean.getHelloWorld());
//			System.out.println("^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#");			
//		} catch (NamingException e) {
//			System.out.println("^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#^#");
//			e.printStackTrace();
//		} 
		
		
		
		System.out.println("FIM");
	}
	
	@Bean
	public RestTemplate rest() {
		return new RestTemplate();
	}
	
	@Bean
	public Context context() throws NamingException {
		System.out.println("########################################### CONTEXT ########################################### ");
		Properties properties = new Properties();
		properties.put("java.naming.factory.initial", "org.wildfly.naming.client.WildFlyInitialContextFactory");

					
		return new InitialContext(properties);			
	}
	
	@Bean
	public HelloStatefulWorld helloStatefulWorld(Context context) throws NamingException {
		System.out.println("########################################### STATEFULL ########################################### ");
		return (HelloStatefulWorld) context.lookup(this.getFullName(HelloStatefulWorld.class));
	}
	
	@Bean
	public HelloStatelessWorld helloStatelessWorld(Context context) throws NamingException {
		System.out.println("########################################### STATELESS ########################################### ");
		return (HelloStatelessWorld) context.lookup(this.getFullName(HelloStatelessWorld.class));
	}
	
	@SuppressWarnings("rawtypes")
	private String getFullName(Class classType) {
		System.out.println("########################################### FULL NAME ########################################### ");
		String moduleName = "ejb:/EAR/";
		String beanName = classType.getSimpleName();
		String viewClassName = classType.getName();
		
//		String moduleName = "ejb:/EAR/";
//		String beanName = "HelloStateFulWorld";
//		String viewClassName = classType.getName();
		
		System.out.println(moduleName + beanName + "!" + viewClassName);
		
		return moduleName + beanName + "!" + viewClassName;
	}
	
	
	
	
}
